﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/* Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
naprednih (sin, cos, log, sqrt...) operacija. */
namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        double Rezultat = 0;
        string oppr = "";
        string oppr1 = "";
        bool isOperationPrefo = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (textBox.Text == "0" || isOperationPrefo)
            {
                textBox.Clear();
            }
            isOperationPrefo = false;
            Button button = (Button)sender;
            if(button.Text == ".")
            {
                if (!textBox.Text.Contains("."))
                    textBox.Text = textBox.Text + button.Text;
            }
            else
                textBox.Text = textBox.Text + button.Text;
        }

        private void buttonOp_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            oppr = button.Text;
            oppr1 = button.Text;
            Rezultat = double.Parse(textBox.Text);
            label2.Text = textBox.Text + " " + oppr;
            isOperationPrefo = true;
        }

        private void buttonOp1_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            oppr = button.Text;
            if (oppr1 != "")
            {
                label2.Text = textBox.Text + " " + oppr1 + " " + oppr;
                
            }
            else if(textBox.Text == "0")
            {
                textBox.Clear();
                label2.Text = oppr;
            }
            else 
            {
                label2.Text = textBox.Text + " " + oppr;
            }
            isOperationPrefo = true;
        }
        private void button18_Click(object sender, EventArgs e)
        {
            switch (oppr)
            {
                case "+":
                   textBox.Text = (Rezultat + double.Parse(textBox.Text)).ToString();
                   break;
                case "-":
                    textBox.Text = (Rezultat - double.Parse(textBox.Text)).ToString();
                    break;
                case "*":
                    textBox.Text = (Rezultat * double.Parse(textBox.Text)).ToString();
                    break;
                case "/":
                    textBox.Text = (Rezultat / double.Parse(textBox.Text)).ToString();
                    break;
                case "sin(":
                    if(oppr1 == "+")
                    {
                        textBox.Text = (Rezultat + Math.Sin(double.Parse(textBox.Text))).ToString();
                    }
                    else if(oppr1 == "-")
                    {
                        textBox.Text = (Rezultat - Math.Sin(double.Parse(textBox.Text))).ToString();
                    }
                    else if(oppr1 == "*")
                    {
                        textBox.Text = (Rezultat * Math.Sin(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "/")
                    {
                        textBox.Text = (Rezultat / Math.Sin(double.Parse(textBox.Text))).ToString();
                    }
                    else {
                        textBox.Text = (Math.Sin(double.Parse(textBox.Text))).ToString();
                    }

                    break;
                case "tg(":
                    if (oppr1 == "+")
                    {
                        textBox.Text = (Rezultat + Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "-")
                    {
                        textBox.Text = (Rezultat - Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "*")
                    {
                        textBox.Text = (Rezultat * Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "/")
                    {
                        textBox.Text = (Rezultat / Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    else
                    {
                        textBox.Text = (Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    break;
                case "cos(":
                    if (oppr1 == "+")
                    {
                        textBox.Text = (Rezultat + Math.Cos(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "-")
                    {
                        textBox.Text = (Rezultat - Math.Cos(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "*")
                    {
                        textBox.Text = (Rezultat * Math.Cos(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "/")
                    {
                        textBox.Text = (Rezultat / Math.Cos(double.Parse(textBox.Text))).ToString();
                    }
                    else
                    {
                        textBox.Text = (Math.Cos(double.Parse(textBox.Text))).ToString();
                    }
                    break;
                case "ctg(":
                    if (oppr1 == "+")
                    {
                        textBox.Text = (Rezultat + 1/Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "-")
                    {
                        textBox.Text = (Rezultat - 1/Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "*")
                    {
                        textBox.Text = (Rezultat * 1/Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "/")
                    {
                        textBox.Text = (Rezultat / 1/Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    else
                    {
                        textBox.Text = (1/Math.Tan(double.Parse(textBox.Text))).ToString();
                    }
                    break;
                case "Sqrt(":
                    if (oppr1 == "+")
                    {
                        textBox.Text = (Rezultat + Math.Sqrt(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "-")
                    {
                        textBox.Text = (Rezultat - Math.Sqrt(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "*")
                    {
                        textBox.Text = (Rezultat * Math.Sqrt(double.Parse(textBox.Text))).ToString();
                    }
                    else if (oppr1 == "/")
                    {
                        textBox.Text = (Rezultat / Math.Sqrt(double.Parse(textBox.Text))).ToString();
                    }
                    else
                    {
                        textBox.Text = (Math.Sqrt(double.Parse(textBox.Text))).ToString();
                    }
                    break;
                default:
                    break;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox.Clear();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Rezultat = 0;
            textBox.Clear();
            oppr1 = "";
            label2.Text = "";
        }

        private void button21_Click(object sender, EventArgs e)
        {
            if (textBox.Text.Length > 1)
                textBox.Text = textBox.Text.Remove(textBox.Text.Length - 1);
            else if (textBox.Text.Length == 1)
            {
                textBox.Text = textBox.Text.Remove(textBox.Text.Length - 1);
                textBox.Text = "0";
            }
        }

       
    }
}
