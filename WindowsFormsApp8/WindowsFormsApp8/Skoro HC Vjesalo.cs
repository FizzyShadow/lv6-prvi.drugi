﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/*Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u
svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala,
dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. */
namespace WindowsFormsApp8
{
    public partial class Form1 : Form
    {

        private Image[] hangman = {WindowsFormsApp8.Properties.Resources.hangman1, WindowsFormsApp8.Properties.Resources.hangman2, WindowsFormsApp8.Properties.Resources.hangman3, WindowsFormsApp8.Properties.Resources.hangman4,
                                    WindowsFormsApp8.Properties.Resources.hangman5, WindowsFormsApp8.Properties.Resources.hangman6, WindowsFormsApp8.Properties.Resources.hangman7};

        private string path = "C:\\Users\\Rerna\\Desktop\\txt.txt";

        private string chosen = "";
        private string copchosen = "";

        private List<string> lista = new List<string>();
        private Random rng = new Random();
        private int broj_Pokusaja;
        private int broj;

        public Form1()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            chosen = lista[rng.Next(0, lista.Count - 1)];
            broj_Pokusaja = 6;
            broj = 1;

            pictureBox1.Image = hangman[0];
            textBox1.Clear();
            label6.Text = broj_Pokusaja.ToString();
            label5.Text = string.Empty;
            copchosen = "";
            for (int i = 0; i < chosen.Length; i++)
            {
                copchosen += "_";

            }
            follow();
            



        }
        public void follow()
        {
            label3.Text = "";
            for( int i = 0; i < copchosen.Length; i++)
            {
                label3.Text += copchosen.Substring(i, 1);
                label3.Text += " ";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

           
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lista.Add(line);
                }
                Reset();
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 1)
            {
                if (chosen.Contains(textBox1.Text))
                {
                    char[] temp = copchosen.ToCharArray();
                    char[] find = chosen.ToCharArray();
                    char charac = textBox1.Text.ElementAt(0);
                    for(int i = 0; i < find.Length; i++)
                    {
                        if(find[i] == charac)
                        {
                           temp[i] = charac;
                        }
                    }
                    copchosen = new string(temp);
                    follow();
                   
                    label5.Text += textBox1.Text;
                    label5.Text += " ";
                    textBox1.Clear();

                }
                else
                {
                    --broj_Pokusaja;
                    pictureBox1.Image = hangman[broj++];
                    label5.Text += textBox1.Text;
                    label5.Text += " ";
                    textBox1.Clear();
                    label6.Text = broj_Pokusaja.ToString();
                    if (broj_Pokusaja == 0)
                    {
                        MessageBox.Show("GAME OVER!!");
                        DialogResult dialogResult = MessageBox.Show("Try Again?", "New Game", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            
                            Reset();
                        }
                        else
                        {
                            Application.Exit();
                        }
                    }
                   
                }    
            }else if(textBox1.Text == chosen)
            {
                textBox1.Clear();
                MessageBox.Show("POBJEDA!!");
            }
            else
            {
                --broj_Pokusaja;
                pictureBox1.Image = hangman[broj++];
                textBox1.Clear();
                if (broj_Pokusaja == 0)
                {
                    MessageBox.Show(string.Format("GAME OVER!!\n{0}", chosen));
                    DialogResult dialogResult = MessageBox.Show("Try Again?", "New Game", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        
                        
                        Reset();
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
                label6.Text = broj_Pokusaja.ToString();
            }
            
        }

       
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
